

// 2

db.fruits.aggregate([
		{$match: {onSale: true}},
		{$count: "FruitsonSale"}


	])



// 3

db.fruits.aggregate([
		{$match: {stock:{$gte:20 }}},
		{$count: "enoughStock"}

	])



// 4

db.fruits.aggregate([
	{$match: {onSale: true}},
		{$group:{_id:"$supplier_id",
		totalFruits:{$avg:"$price"}}},
	])



// 5

db.fruits.aggregate([
	{$match: {onSale: true}},
		{$group:{_id:"$supplier_id",
		totalFruits:{$max:"$price"}}},
		{$sort:{totalFruits:1}}
	])


// 6

db.fruits.aggregate([
	{$match: {onSale: true}},
		{$group:{_id:"$supplier_id",
		totalFruits:{$min:"$price"}}},
		{$sort:{totalFruits:1}}
	])